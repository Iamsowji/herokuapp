package com.herokuapp;

import org.testng.annotations.Test;

public class AllTestCases extends AbstractTest 
{
	@Test
	public void AddCheckBox()
	{
		addcheckbox.clickon_dynamicControls();
		sl.wait_nextPage();
		addcheckbox.remove_checkbox();//defaul it is enabled as remove button
		sl.sleep_nextPage();
		addcheckbox.add_checkbox();
	}
	@Test
	public void RemoveCheckBox()
	{
		removecheckbox.Signin_clickon_dynamic_controls();
		sl.wait_nextPage();
		removecheckbox.clickon_removebutton();
		
	}
	@Test
	public void Login_validdata()
	{
		loginvaliddata.clickon_login_button();
	}
	@Test
	public void Login_Invaliddata()
	{
		logininvaliddata.invalidlogin();
	}

}
