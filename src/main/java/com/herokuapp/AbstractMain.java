package com.herokuapp;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.Lib.BusinessLibrary;
import com.Lib.SystemLibrary;
import com.pages.AddCheckBox;
import com.pages.DynamicLoading;
import com.pages.LoginIn_InValidData;
import com.pages.LoginIn_ValidData;
import com.pages.RemoveCheckBox;

public class AbstractMain {

		public static WebDriver driver=new FirefoxDriver();
		public static AddCheckBox addcheckbox= new AddCheckBox();
		public static RemoveCheckBox removecheckbox=new RemoveCheckBox();
		public static DynamicLoading dynamicloading=new DynamicLoading();
		public static LoginIn_ValidData loginvaliddata = new  LoginIn_ValidData();
		public static LoginIn_InValidData logininvaliddata =new LoginIn_InValidData();
		public static BusinessLibrary bl =new BusinessLibrary();
		public static SystemLibrary sl=new SystemLibrary();
		
		

	}


