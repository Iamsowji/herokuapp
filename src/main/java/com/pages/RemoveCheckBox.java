package com.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;

import com.herokuapp.AbstractMain;

public class RemoveCheckBox extends AbstractMain
{
	public void Signin_clickon_dynamic_controls()
	{
	
	driver.get("http://the-internet.herokuapp.com");
	driver.findElement(By.xpath("//*[@id='content']/ul/li[11]/a")).click();
	driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
	}
	public void clickon_removebutton()
	{
	driver.findElement(By.xpath("//*[@id='btn']")).click();
	}
}
